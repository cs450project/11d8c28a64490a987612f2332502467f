#
# Makefile for MPX

AS	= nasm
CC	= i386-elf-gcc
CFLAGS  = -Wall -Wextra -Werror -nostartfiles -nodefaultlibs -nostdlib -g -c
LD	= i386-elf-ld
LDFLAGS = -T link.ld
ASFLAGS = -f elf -g

OBJFILES =\
boot/loader.o\
kernel/kernel.o

LIBS =\
lib/lib.o

MODULES =\
modules/modules.o

all: kernel.img

.s.o:
	$(AS) $(ASFLAGS) -o $@ $<
.c.o:
	$(CC) $(CFLAGS) -I./include -I./modules -o $@ $<

.PHONY : kernel/kernel.o
kernel/kernel.o:
	(cd kernel ; make)

.PHONY : lib/lib.o
lib/lib.o:
	(cd lib ; make)

.PHONY : modules/modules.o
modules/modules.o:
	(cd modules ; make)

.PHONY : kernel.bin
kernel.bin: $(OBJFILES) $(LIBS) $(MODULES)
	$(LD) $(LDFLAGS) -o $@ $(OBJFILES) $(LIBS) $(MODULES)

.PHONY : kernel.img
kernel.img: kernel.bin
	dd if=/dev/zero of=pad bs=1 count=750
	cat boot/grub/stage1 boot/grub/stage2 pad kernel.bin > $@

clean:
	(cd kernel ; make clean)
	(cd lib ; make clean)
	(cd modules ; make clean)
	rm $(OBJFILES) $(LIBS) kernel.bin kernel.img pad

backup: clean
	(cd .. ; sh backup.sh ; cd mpx2012)
