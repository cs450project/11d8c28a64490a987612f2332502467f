
# Prototyping of a memory allocator

class Mem():
    def __init__(self, size):
        self.size = size

class Block():
    def __init__(self, start, size):
        self.header = Header(start,size)
        self.block = Mem(size)
        self.footer = Footer(start+Header.size+size,self.header)
        self.size = size + Header.size + Footer.size

    def __repr__(self):
        return 'H(%s,%s):M:F(%s,H)' % (self.header.addr,self.size,self.footer.addr)

class Header():
    size = 4
    def __init__(self, addr, size):
        self.addr = addr
        self.size = size

class Footer():
    size = 4
    def __init__(self, addr, header):
        self.addr = addr
        self.header = header

class IndexTable():
    def __init__(self, size, minalloc, maxsize):
        self.rec = {'size':0,'empty':1,'ptr':0}
        self.table = [self.rec.copy() for _ in xrange(maxsize/(Header.size + 4 + Footer.size))]
        self.table[0]['size'] = size;

    def __len__(self):
        return len(self.table)

    def __repr__(self):
        return str([(_['size'],_['empty'],_['ptr']) for _ in self.table])

    def find(self, mem):
        for rec in self.table:
            if rec['ptr'] == mem:
                return rec

    def find_empty(self, size):
        for rec in self.table:
            if rec['empty'] == 1:
                if rec['size'] >= size:
                    return rec

    def merge_empty(self):
        empty = [rec for rec in self.table 
                 if rec['ptr'] == 0
                 and rec['size'] != 0]
        [self.table.remove(e) for e in empty]

        length = len(empty)
        if length==0: return

        merged_empty = self.rec.copy()
        merged_empty['size'] = sum(e['size'] for e in empty)
        self.table.append(merged_empty);
        [self.table.append(self.rec.copy()) for _ in xrange(length-1)]

        self.sort()

    def sort(self):
        self.table.sort(lambda x,y: cmp(x['size'],y['size']))
        head = self.table[0]
        while(head==self.rec):
            self.table.remove(head)
            self.table.append(head)
            head = self.table[0]

    def split(self, old_rec, size, new_block=None):

        rec,i = [(r,i) for i,r in enumerate(self.table) if r == old_rec][0]

        if i < len(self.table):
            
            #increase size of next rec
            #if self.table[i+1]['empty'] == 1:
            #    self.table[i+1]['size'] += rec['size']-size
                #rec['size'] = size
            
            #create new rec
            if self.rec in self.table:
                newrec = [r for r in self.table if r==self.rec][0]
                newrec['size'] = rec['size']-size
                newrec['ptr'] = new_block
                rec['size'] = size

        else: pass #do nothing
        
        self.sort();

class Heap():
    def __init__(self, start, maxsize, minsize):
        self.start = start
        self.maxsize = maxsize
        self.end = self.start + self.maxsize
        self.allocated = minsize + Header.size + Footer.size
        self.index = IndexTable(self.allocated,(Header.size+Footer.size),maxsize)
#        self.mem = [Header(start,minsize),
#                    Mem(minsize + Header.size + Footer.size),
#                    Footer(start+minsize+Header.size+Footer.size, Header(start,minsize))]

        self.mem = [Block(start,minsize)]
        self.index.table[0]['ptr'] = self.mem[0]

    def alloc(self, size):
        blocksize = size + Header.size + Footer.size
        rec = self.index.find_empty(blocksize)

        if rec == None:
            return -1

        if rec['size'] > size:
            self.split(rec, blocksize)

        rec['empty'] = 0;

        #addr = rec['ptr'].addr;
        #rec['ptr'] = Block(addr, size)
        #self.index.merge_empty()
        self.allocated += blocksize

        return rec['ptr']

    def free(self, mem):
        rec = self.index.find(mem)
        if rec == None:
            print 'didn\'t find it'
            return
        else:
            rec['ptr'] = 0
            self.allocated -= rec['size']
            #self.index.merge_empty()

    def grow_heap(self,addr):
        print "growing heap"

    def split(self, rec, blocksize):
        #if not(self.allocated + blocksize >= self.maxsize):
        #    self.mem.append
        newblock = Block(rec['ptr'].header.addr+blocksize, rec['ptr'].size-blocksize)
        self.mem.append(newblock)
        self.index.split(rec, blocksize, newblock)
        rec['size'] = blocksize
        rec['ptr'].size = blocksize;
        rec['ptr'].header.size = blocksize
        #rec['ptr'].footer.addr = rec['ptr'].header.addr + blocksize - Footer.size
        rec['ptr'].footer.addr = rec['ptr'].header.addr + blocksize - Footer.size

        if rec['ptr'].footer.addr > self.start + self.allocated:
            self.grow_heap(rec['ptr'].footer.addr)

def main():
    heap = Heap(200,64,32)
    test_heap(heap)

def test_heap(heap):

    def print_heap():
        print heap.index
        
        out = []
        for m in heap.mem:
            # if isinstance(m,Header):
            #     out.append( 'H:(%s,%s)' % (m.addr,m.size) )
            # elif isinstance(m,Mem):
            #     out.append( 'M' )
            # elif isinstance(m,Footer):
            #     out.append( 'F:(%s,H)' % (m.addr) )
            out.append('H(%s,%s):M:F(%s,H)' % (m.header.addr,m.size,m.footer.addr))
        print out

        print 'length: %d' % len(heap.index)
        print 'heapsize: %d' % heap.allocated
        print '\n'

    print_heap()

    mem0 = heap.alloc(4)
    print "mem0: %s" % mem0.size
    print_heap()

    mem1 = heap.alloc(4)
    print "mem1: %s" % mem1.size
    print_heap()

    mem2 = heap.alloc(4)
    print "mem2: %s" % mem2.size
    print_heap()

    # heap.free(mem1)
    # print mem1.size
    # print_heap()

    # heap.free(mem2)
    # print mem2.size
    # print_heap()

    # heap.free(mem0)
    # print mem0.size
    # print_heap()

if __name__=='__main__':
    main()
