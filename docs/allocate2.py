class Mem():
    def __init__(self, addr, size):
        self.addr = addr
        self.size = size

    def __repr__(self):
        return 'Mem(%s,%s)' % (self.addr, self.size)

class Header():
    size = 4
    def __init__(self, addr, size, index):
        self.addr = addr
        self.size = size
        self.id = index

class Footer():
    size = 4
    def __init__(self, addr, header):
        self.addr = addr
        self.header = header

class Block():
    min_size = 12 # (Header.size + 4 + Footer.size)
    def __init__(self, addr, size, index):
        self.addr = addr
        self.header = Header(addr, size+4+4, index)
        self.mem = Mem(addr+4, size)
        self.footer = Footer(addr+4+size, self.header)
        self.size = size+4+4

    def __repr__(self):
        return 'Block<H(%s:%s:%s) M(%s:%s) F(%s,H)>' % (self.header.addr,
                                                        self.header.size,
                                                        self.header.id,
                                                        self.mem.addr,
                                                        self.mem.size,
                                                        self.footer.addr)
    def join(self, other):
        return Block(self.addr,
                     self.size-4 + other.size-4,
                     -1)
    def split(self):
        return (Block(self.addr,
                      self.size/2-8,
                      -1),
                Block(self.addr + self.size/2,
                      self.size/2-8,
                      -1))

class IndexTable():
    def __init__(self, max_size):
        self.rec = {'size':  0, 
                    'empty': 1,
                    'block': 0}
        self.table = [self.rec.copy()
                      for _ in xrange(max_size/Block.min_size)]
        self.id = 0

    def __len__(self):
        return len(self.table)

    def __tostr__(self):
        return self.__repr__()

    def __repr__(self):
        out = '\n'
        return out.join(["Entry(%s,%s,%s)" % (_['size'],_['empty'],_['block'])
                    for _ in self.table])

    def add(self, block, merge=1, heap=None):
        for rec in self.table:
            if rec == self.rec:
                rec['size'] = block.size
                block.header.id = self.id
                rec['block'] = block
                self.id += 1
                if merge: self.merge_empty(heap)
                return

    def find(self, mem):
        # use header addr-sizeof(header)
        for rec in self.table:
            if rec['block'].mem == mem:
                return rec

    def find_empty(self, size):
        for rec in self.table:
           if (rec['empty'] == 1) and (rec['size'] >= size):
               return rec
        return -1

    def merge_empty(self, mem):
        empty = lambda x: [rec for rec in self.table
                           if rec['empty'] == 1 and rec['block'] != 0]
        
        for rec in empty(0):
            for rec2 in empty(0):
                if rec['block'].footer.addr == rec2['block'].header.addr - 4:
                    nb = rec['block'].join(rec2['block'])
                    self.remove(rec,0)
                    mem.remove(rec['block'])
                    self.remove(rec2,0)
                    mem.remove(rec2['block'])
                    self.add(nb,0)
                    mem.append(nb)
                    self.merge_empty(mem)
                    return
                elif rec2['block'].footer.addr == rec['block'].header.addr - 4:
                    nb = rec2['block'].join(rec['block'])
                    self.remove(rec,0)
                    mem.remove(rec['block'])
                    self.remove(rec2,0)
                    mem.remove(rec2['block'])
                    self.add(nb,0)
                    mem.append(nb)
                    self.merge_empty(mem)
                    return

    def sort(self):
        self.table.sort(lambda x,y: cmp(x['size'],y['size']))
        head = self.table[0]
        while(head==self.rec):
            self.table.remove(head)
            self.table.append(head)
            head = self.table[0]

    def remove(self, rec, merge=1, heap=None):
        self.table.remove(rec)
        self.table.append(self.rec.copy())
        if merge: self.merge_empty(heap)

class Heap():
    def __init__(self, start, max_size, min_size):
        self.start = start
        self.max_size = max_size
        self.min_size = min_size
        self.end = self.start + self.max_size
        
        self.mem = [Block(start,min_size,0)]
        self.allocated = min_size + Header.size + Footer.size
        
        self.index = IndexTable(max_size)
        self.index.add(self.mem[0])

    def __repr__(self):
        out = '\n'
        return "HEAP:\n" + out.join([str(b) for b in self.mem])

    def alloc(self, size):
        
        blocksize = size + Header.size + Footer.size
        rec = self.index.find_empty(blocksize)

        if rec == -1:
            if self.allocated + blocksize > self.max_size:
                return -1
            else:
                self.grow_heap(blocksize)
                return self.alloc(size)

        # split mem, recurse
        if rec['size']/2 >= blocksize:
            self.index.remove(rec,0)
            nb1,nb2 = rec['block'].split()
            self.mem.remove(rec['block'])
            self.mem.append(nb1)
            self.mem.append(nb2)
            self.index.add(nb1,0)
            self.index.add(nb2,0)
            return self.alloc(size)

        rec['empty'] = 0
        self.index.merge_empty(self.mem)
        return rec['block'].mem

    def free(self, mem):
        rec = self.index.find(mem)
        rec['empty']=1
        self.index.merge_empty(self.mem)
        
    def grow_heap(self, size):
        rec = self.index.table[-1]
        rec['size'] = size;
        self.allocated += size
        rec['block'] = Block(self.start + self.allocated - size, size, self.index.id)
        
        self.index.id+=1
        self.index.merge_empty(self.mem)
        
        self.index.sort()
        self.mem.append(rec['block'])
        
def main():
    
    heap_test0()
    heap_test1()

    h = Heap(200, 128, 24)

    print h.index, h

    mem0 = h.alloc(32)
    print mem0
    print h.index, h

    mem1 = h.alloc(32)
    print mem0
    print h.index, h

    mem2 = h.alloc(32)
    print mem2
    if mem2 != -1: testpass = 0

    h.free(mem0)
    print h.index, h

def heap_test1():
    
    testpass = 1
    h = Heap(200, 128, 56)

    mem0 = h.alloc(3)
    h.free(mem0)
    if len(h.mem)!=1: testpass = 0

    mem1 = h.alloc(8)
    mem2 = h.alloc(8)
    mem3 = h.alloc(8)
    if len(h.mem)!=3: testpass = 0

    h.free(mem1)
    if len(h.mem)!=3: testpass = 0
    h.free(mem2)
    if len(h.mem)!=2: testpass = 0
    h.free(mem3)
    if len(h.mem)!=1: testpass = 0

    if testpass:
        print "Heap Test 1: PASS"
    else:
        print "Heap Test 1: FAIL"


def heap_test0():

    testpass = 1    
    h = Heap(200, 128, 56)
    mem0 = h.alloc(8)
    mem1 = h.alloc(8)
    mem2 = h.alloc(16)
    h.free(mem2)
    h.free(mem1)
    h.free(mem0)
    if len(h.mem)!=1: testpass = 0

    if testpass:
        print "Heap Test 0: PASS"
    else:
        print "Heap Test 0: FAIL"

if __name__=='__main__':
    main()
