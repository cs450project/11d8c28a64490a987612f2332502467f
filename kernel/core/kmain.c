/*
  ----- kmain.c -----

  Description..: Kernel main. The first function called after
      the bootloader. Initialization of hardware, system
      structures, devices, and initial processes happens here.
*/

#include <stdint.h>
#include <string.h>
#include <system.h>

#include <core/io.h>
#include <core/serial.h>
#include <core/tables.h>
#include <core/interrupts.h>
#include <mem/heap.h>
#include <mem/paging.h>

#include "modules/mpx_supt.h"
#include "modules/commhand.h"
#include "modules/pcb.h"
#include "modules/queue.h"
#include "modules/sched.h"

// Currently Operating Process
extern pcb* cop;

// Kernel Tasks
void idle()
{
  while(1){
    sys_req(IDLE);
  }
}

void kmain(void)
{
   extern uint32_t magic;
 
   // Uncomment if you want to access the multiboot header
   // extern void *mbd;
   // char *boot_loader_name = (char*)((long*)mbd)[16];

   // 0) Initialize Serial I/O
   init_serial(COM1);
   init_serial(COM2);
   set_serial_out(COM1);
   set_serial_in(COM1);
   klogv("Starting MPX boot sequence...");
   klogv("Initialized serial I/O on COM1 device...");

   // 1) Check that the boot was successful and correct when using grub
   // Comment this when booting the kernel directly using QEMU, etc.
   if ( magic != 0x2BADB002 ){
     //kpanic("Boot was not error free. Halting.");
   }
   
   // 2) Descriptor Tables
   klogv("Initializing descriptor tables...");
   init_gdt();
   init_idt();

   // 3) PIC, ISRs
   klogv("Enabling interrupts...");
   init_pic();
   init_irq();
   sti();

   // Check if interrupts are enabled
   if (irq_on()){
     klogv("Interrupts are enabled...");
   }
   else {
     kpanic("Couldn't enable interrupts. Halting.");
   }

   // 4) Virtual Memory
   klogv("Initializing virtual memory...");
   init_paging();   
   //init_heap();

   // 5) Console
   klogv("Initializing console...");
   init_commhand();

   // 6) Initialize Devices

   // Example code for using serial interrupts:

   //COM1
   //cli();
   //outb(0x21,inb(0x21)&~0x10); //enable irqs on COM1 input
   //outb(COM1+4, 0x08); //enable serial interrupts
   //outb(COM1+1, 0x01); //enable input ready interrupt
   //sti();

   //COM2
   //cli();
   //outb(0x21,inb(0x21)&~0x08); //enable irqs on COM2 input
   //outb(COM2+4, 0x08); //enable serial interrupts
   //outb(COM2+1, 0x01); //enable input ready interrupt
   //sti();

   // 7) Scheduler
   klogv("Initializing scheduler...");
   init_sched();
   init_queues();

   // 8) Install Task0 (init/idle)
   pcb *p0 = create_pcb("idle", SYSTEM, READY, 0, 256);
   context* cp = (context*)(p0->stack_top);
   memset(cp, 0, sizeof(context));
   cp->fs = 0x10;
   cp->gs = 0x10;
   cp->ds = 0x10;
   cp->es = 0x10;
   cp->cs = 0x8;
   cp->ebp = (u32int)(p0->stack);
   cp->esp = (u32int)(p0->stack_top);
   cp->eip = (u32int)idle;
   cp->eflags = 0x202;
   insert_pcb(p0);

   // 9) Install Command Handler
   pcb *commhand_p = create_pcb("commhand", SYSTEM, READY, 4, 10000);
   cp = (context*)(commhand_p->stack_top);
   memset(cp, 0, sizeof(context));
   cp->fs = 0x10;
   cp->gs = 0x10;
   cp->ds = 0x10;
   cp->es = 0x10;
   cp->cs = 0x8;
   cp->ebp = (u32int)(commhand_p->stack);
   cp->esp = (u32int)(commhand_p->stack_top);
   cp->eip = (u32int)commhand;
   cp->eflags = 0x202;
   insert_pcb(commhand_p);

   // 10) Call Scheduler
   klogv("End kmain(). Transferring control to the scheduler...");
   asm volatile ("int $60");

   // 11) System Shutdown
   klogv("Starting system shutdown procedure...");
   
   /* Shutdown Procedure */
   
   klogv("Shutdown complete. You may now turn off the machine. (QEMU: C-a x)");
   hlt();
}
