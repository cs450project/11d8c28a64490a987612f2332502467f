import sys
import argparse
import hashlib
import json
import urllib
import urllib2
import urlparse

from getpass import getpass

def chown(groupname, leadername, args):
    repositoryname = hashlib.md5(groupname).hexdigest()
    data = urllib.urlencode({'permission':'admin'})
    req = urllib2.Request(args.base_url+'invitations/'+args.userpass[0]+'/'+repositoryname+'/'+leadername, data, args.auth)
    print("Setting owner of '%s'for group '%s' to '%s'" % (repositoryname,groupname,leadername))
    res = urllib2.urlopen(req).read()
    return res

def firstcommit(groupname, args):
    pass

def mkreadme(groupname):
    pass

def mkrepo(groupname, args):
    repositoryname = hashlib.md5(groupname).hexdigest()
    data = urllib.urlencode({'name':repositoryname,'scm':'git','description':groupname})
    req = urllib2.Request(args.base_url+'repositories/', data, args.auth)
    print("Creating repository '%s' for group '%s'" % (repositoryname,groupname))
    res = urllib2.urlopen(req).read()
    return res

def rmrepo(groupname, args):
    repositoryname = hashlib.md5(groupname).hexdigest()
    req = urllib2.Request(args.base_url+'repositories/'+args.userpass[0]+'/'+repositoryname, None, args.auth)
    req.get_method = lambda: 'DELETE'
    print("Removing repository '%s' for group '%s'" % (repositoryname,groupname))
    res = urllib2.urlopen(req).read()
    return res

def mkauth(username, password):
    encodedstring = ':'.join((username,password)).encode('base64').strip()
    return {'Authorization':'Basic ' + encodedstring}

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('groups_list')
    parser.add_argument('--clean',
                        dest='clean',
                        action='store_const',
                        const=True,
                        help='delete the repositories corresponding to a groups list')
    parser.add_argument('--salt',
                        dest='salt',
                        action='store')
                        
    args = parser.parse_args()
    args.userpass = 'cs450project','450.D79712494' #getpass()
    args.auth = mkauth(*args.userpass)
    args.base_url = 'https://api.bitbucket.org/1.0/'

    with open(args.groups_list) as fp:
        lines   = filter(lambda x: x!='', [l.strip() for l in fp.readlines()])
        groups  = lines[::2]
        leaders = lines[1::2]

    salt = args.salt or 'cs450'

    if args.clean:
        [rmrepo(group, args) for group in groups]
    else:
        [mkrepo(group, args, salt) for group in groups]
        [chown(group, leader, args) for group,leader in zip(groups,leaders)]
        print '''
Finished generating repositories.
Emails have been dispatched to team leaders.
'''

if __name__=='__main__':
    main()
