#ifndef _MPX_SUPT_H
#define _MPX_SUPT_H

#define EXIT 0
#define IDLE 1
#define READ 2
#define WRITE 3

typedef struct {
  int op_code;
  int device_id;
} param;

int sys_req( int op_code );

#endif
