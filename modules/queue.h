#ifndef _QUEUE_H
#define _QUEUE_H

#include "pcb.h"

/* PCB Queues */
typedef struct {
  pcb* head;
  pcb* tail;
  int count;
} process_queue;

void clear_queues();
void init_queues();
void insert_pcb(pcb* new);
void remove_pcb(pcb* p);
void block_pcb(pcb* p);
void resume_pcb(pcb *p);
void suspend_pcb(pcb* p);
void unblock_pcb(pcb* p);
pcb* find_pcb(const char* name);
process_queue* get_queue(const int status);

#endif
