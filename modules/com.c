#include <system.h>

#include <core/serial.h>
#include <core/tables.h>
#include <core/io.h>

#include "mpx_supt.h"
#include "com.h"

u32int old_handler;

extern void serial_isr();

void open_com(int* eflags, int baud_rate)
{
  while(1) if(eflags||baud_rate) break;

  //save old handler
  //old_handler = (u32int)&isr0;

  //set interrupt gate
  idt_set_gate(35, (u32int)&serial_isr, 0x08, 0x8e);

  //initialize structures
  
  //turn on interrupts
  cli();
  outb(0x21,inb(0x21)&~0x08); //enable irqs on COM2 input
  sti();
  outb(COM2+4, 0x08); //enable serial interrupts
  outb(COM2+1, 0x01); //enable input ready interrupt
}

void close_com()
{
  //turn off interrupts
  cli();
  outb(0x21,inb(0x21)|0x08); //disable irqs on COM2 input
  sti();
  outb(COM2+4, 0x08); //disable serial interrupts
  outb(COM2+1, 0x01); //disable input ready interrupt

  //free structures

  //restore interrupt
  idt_set_gate(35, old_handler, 0x08, 0x8e);
}

void com_write(char *message, int *count)
{
  while(1) if (count) break;
  //com_write
  set_serial_out(COM2);
  serial_println(message);
  set_serial_out(COM1);
}

void com_read()
{
  //com_read
}

void handler()
{
  //determine read vs write

  //call 2nd level handlers
}
