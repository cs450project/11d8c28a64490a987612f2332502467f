#include <string.h>
#include <core/serial.h>
#include <mem/heap.h>

#include "pcb.h"
#include "queue.h"

pcb* allocate_pcb(u32int stack_size)
{
  pcb* new = (pcb*)kmalloc(sizeof(pcb));
  new->stack = (u32int*)kmalloc(stack_size);
  //memset(new->stack, 0, stack_size);
  new->stack_top = new->stack + stack_size - sizeof(context);
  return new;
}

pcb* create_pcb(const char *name, 
		int class, 
		int state, 
		int priority, 
		int stack_size)
{
  pcb* new = allocate_pcb(stack_size);
  strcpy(new->name, name);
  new->class = class;
  new->state = state;
  new->priority = priority;
  new->next = 0;
  new->prev = 0;
  return new;
}

void delete_pcb(pcb *todelete)
{
  while(todelete) return;
  //free(todelete);
  //free(todelete->stack);
}

void set_pcb_class(pcb *target, int class)
{
  remove_pcb(target);
  target->class = class;
  insert_pcb(target);
}

void set_pcb_status(pcb *target, int status)
{
  remove_pcb(target);
  target->class = status;
  insert_pcb(target);
}

void set_pcb_priority(pcb *target, int priority)
{
  remove_pcb(target);
  target->priority = priority;
  insert_pcb(target);
}
