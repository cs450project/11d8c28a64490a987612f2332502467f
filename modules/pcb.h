#ifndef _PCB_H
#define _PCB_H

#define ALL 5

/* Task States */
#define RUNNING 0
#define READY 1
#define SREADY 2
#define BLOCKED 3
#define SBLOCKED 4

/* Task Classes */
#define SYSTEM 0
#define APPLICATION 1

//old context definition
/* typedef struct { */
/*   u32int BP, DI, SI, DS, ES; */
/*   u32int DX, CX, BX, AX; */
/*   u32int IP, CS, eflags; */
/* } context; */

typedef struct {
  u32int gs, fs, es, ds;
  u32int edi, esi, ebp, esp, ebx, edx, ecx, eax;
  u32int eip, cs, eflags;
} context;

typedef struct pcb_s {
  char name[32];
  int class;
  int state;
  int priority;
  u32int *stack;
  u32int *stack_top;
  struct pcb_s *next;
  struct pcb_s *prev;
} pcb;

pcb* create_pcb(const char *name, 
		int class, 
		int state, 
		int priority,
		int stack_size);

#endif
