#include <string.h>
#include <core/serial.h>
#include <mem/heap.h>

#include "pcb.h"
#include "queue.h"

process_queue Ready_q, sReady_q, Blocked_q, sBlocked_q;
process_queue *Ready, *sReady, *Blocked, *sBlocked;

void block_pcb(pcb* p)
{
  remove_pcb(p);
  if (p->state == READY){
    p->state = BLOCKED;
  }
  else if (p->state == SREADY){
    p->state = SBLOCKED;
  }
  insert_pcb(p);
}

void clear_queues()
{
  while(Ready->head){ remove_pcb(Ready->head); }
  while(sReady->head){ remove_pcb(sReady->head); }
  while(Blocked->head){ remove_pcb(Blocked->head); }
  while(sBlocked->head){ remove_pcb(sBlocked->head); }
}

void init_queues()
{
  Ready = &Ready_q;
  sReady = &sReady_q;
  Blocked = &Blocked_q;
  sBlocked = &sBlocked_q;

  Ready->head = 0; Ready->tail = 0; Ready->count = 0;
  sReady->head = 0; sReady->tail = 0; sReady->count = 0;
  Blocked->head = 0; Blocked->tail = 0; Blocked->count = 0;
  sBlocked->head = 0; sBlocked->tail = 0; sBlocked->count = 0;
}

process_queue* get_queue(int state)
{
  switch(state){
  case READY: return Ready;
  case SREADY: return sReady;
  case BLOCKED: return Blocked;
  case SBLOCKED: return sBlocked;
  }
  return 0;
}

void insert_pcb(pcb *newpcb)
{
  process_queue *queue;
  pcb *iter;

  queue = get_queue(newpcb->state);
  iter = queue->head;
  
  //insert into empty queue
  if (!iter){
    queue->head = newpcb;
    queue->tail = newpcb;
    newpcb->next = NULL;
    newpcb->prev = NULL;
  }
  else if (newpcb->state == READY){
    
    //insert at head of ready queue
    if (newpcb->priority > iter->priority){
      queue->head = newpcb;
      newpcb->next = iter;
      newpcb->prev = NULL;
      iter->prev = newpcb;
    }
    //insert after head of ready queue
    else {
      while( (iter->next) && (iter->next->priority >= newpcb->priority) )
	iter = iter->next;

      //middle
      if (iter->next){
	newpcb->next = iter->next;
	newpcb->next->prev = newpcb;
	iter->next = newpcb;
	newpcb->prev = iter;
      } 
      //tail
      else {
	queue->tail->next = newpcb;
	newpcb->prev = queue->tail;
	queue->tail = newpcb;
	newpcb->next = NULL;
      }
    }
  }
  //just insert at tail for other queues
  else {
    queue->tail->next = newpcb;
    newpcb->prev = queue->tail;
    queue->tail = newpcb;
    newpcb->next = NULL;
  }
  queue->count++;
}

void remove_pcb(pcb *toremove)
{
  process_queue *queue = get_queue(toremove->state);
  pcb *iter;

  //only item in queue
  if (queue->head == toremove && queue->tail == toremove){
    queue->head = NULL;
    queue->tail = NULL;
    queue->count = 0;
  }
  //remove at head of queue
  else if (queue->head == toremove){
    queue->head = toremove->next;
    queue->head->prev = NULL;
  }
  //remove at tail of queue
  else if (queue->tail == toremove){
    queue->tail = toremove->prev;
    queue->tail->next = NULL;
  }
  //remove from middle of queue
  else {
    iter = queue->head;
    while(iter != toremove)
      iter = iter->next;
    toremove->prev->next = toremove->next;
    toremove->next->prev = toremove->prev;
  }
  toremove->next = NULL;
  toremove->prev = NULL;
  queue->count--;
}

pcb* find_pcb(const char* name)
{  
  process_queue *queue;
  pcb *iter;
  int qid;

  for (qid=1; qid<5; qid++){
    queue = get_queue(qid);
    iter = queue->head;
    while(iter != 0){
      if (strcmp(iter->name,name)==0)
	return iter;
      iter = iter->next;
    }
  }
  return 0;
}

void resume_pcb(pcb* p)
{
  remove_pcb(p);
  if (p->state == SBLOCKED){
    p->state = BLOCKED;
  }
  else if (p->state == SREADY){
    p->state = READY;
  }
  insert_pcb(p);
}

void suspend_pcb(pcb* p)
{
  remove_pcb(p);
  if (p->state == BLOCKED){
    p->state = SBLOCKED;
  }
  else if (p->state == READY){
    p->state = SREADY;
  }
  insert_pcb(p);
}

void unblock_pcb(pcb* p)
{
  remove_pcb(p);
  if (p->state == BLOCKED){
    p->state = READY;
  }
  else if (p->state == SBLOCKED){
    p->state = SREADY;
  }
  insert_pcb(p);
}
