#ifndef _R1_H
#define _R1_H

#include "commands.h"

/*
  Procedure..: commhand
  Description..: MPX command handler. Reads and evaluates user
     input from the default I/O device.
*/
void commhand(void);

/* Procedure..: commhand_cleanup */
void commhand_cleanup(void);

/* Procedure..: init_commhand */
void init_commhand(void);

/* 
   Procedure..: valid_command
   Description..: Searches through the array of system commands
      and then through the array of user defined aliases. If found,
      returns the command structure, NULL otherwise.
*/
command* valid_command(const char *cmd_buf);

/*
  Procedure..: eval_command
  Description..: Takes a pointer to a command structure and 
     the user input. It then counts the arguments and passes
     the number of arguments (argc) and the user input (argv)
     to the function inside the command structure.
*/
int eval_command(const command *cmd, const char *cmd_buf);

/*
  Procedure..: nth_tok
  Description..: Returns the nth token of a character array using
      the given delimeter as the splitting character.
*/
char* nth_tok(const char *s, const int n, const char *delim);

#endif
