#ifndef _MODULES_H
#define _MODULES_H

/* System Version */
#define SYS_VERSION "2.3"

/* System Name */
#define SYS_NAME "MPX"

/* Maximum Command Length */
#define MAX_CMD_LEN 64

/* Maximum Length of Command Description */
#define MAX_CMD_DESC 64

/* Maximum Number of Commands */
#define MAX_CMD_NUM 64

/* Maximum Number of Aliases */
#define MAX_ALIAS_NUM 8

/* Command History Length */
#define CMD_HIST_LEN 8

#endif
