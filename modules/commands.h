#ifndef _COMMANDS_H
#define _COMMANDS_H

#include "modules.h"

typedef struct cmd_t {
  char name[MAX_CMD_LEN];
  char desc[MAX_CMD_DESC];
  int (*func)();
} command;

typedef struct alias_t {
  char name[MAX_CMD_LEN];
  command *cmd;
} alias;

void add_command(const char *name, const char *desc, int (*func)());
void init_commands();

#define _cmd_args const int argc, const char *cmd_buf

//R1 Commands
int cmd_alias       (_cmd_args);
int cmd_help        (_cmd_args);
int cmd_prompt      (_cmd_args);
int cmd_shutdown    (_cmd_args);
int cmd_version     (_cmd_args);

//R2 Commands
int cmd_create_pcb  (_cmd_args);
int cmd_delete_pcb  (_cmd_args);
int cmd_show        (_cmd_args);
int cmd_show_pcb    (_cmd_args);
int cmd_block_pcb   (_cmd_args);
int cmd_resume_pcb  (_cmd_args);
int cmd_suspend_pcb (_cmd_args);
int cmd_unblock_pcb (_cmd_args);

//R3 Commands
int cmd_load_proc   (_cmd_args);
int cmd_yield       (_cmd_args);
int cmd_loadr3      (_cmd_args);

//R5 Commands
int cmd_load_comw   (_cmd_args);

#endif
