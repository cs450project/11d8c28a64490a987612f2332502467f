#ifndef _COM_H
#define _COM_H

void open_com(int *eflags, int baud_rate);
void close_com();
void com_write(char *message, int* count);
void com_read();
void handler();

#endif
