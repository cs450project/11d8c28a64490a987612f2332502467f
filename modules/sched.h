#ifndef _SCHED_H
#define _SCHED_H

#include "pcb.h"

void dispatch();
u32int* sys_call(context *regs);
void init_sched();

#endif
