#include <string.h>
#include <system.h>

#include <core/io.h>
#include <core/serial.h>
#include <core/tables.h>
#include <mem/heap.h>

#include "mpx_supt.h"
#include "pcb.h"
#include "queue.h"
#include "sched.h"

/* System call default isr, defined in irq.s */
extern void sys_call_isr();

/* System call params, defined in mpx_supt.c */
extern param params;

pcb *cop = 0;
pcb *next = 0;
context *old;
u32int ss_save;
u32int sp_save;
context *cp;
//params *;

/* Kernel process queues */
extern process_queue *Ready, *sReady, *Blocked, *sBlocked;

/*
  Procedure..: init_sched
  Description..: Initialize the scheduler by setting the appropriate
     gate in the interrupt descriptor table. Also creates storage for 
     the original caller's registers.
*/
void init_sched()
{
  idt_set_gate(60, (u32int)sys_call_isr, 0x08, 0x8e);
  old = (context*)kmalloc(sizeof(context));
  memset(old, 0, sizeof(context));
}

/*
  Procedure..: sys_call
  Description..: Default system call handler. Handles scheduling and
      context switching. Will handle I/O scheduling as well.
*/
u32int* sys_call(context *registers)
{
  // Get next process to dispatch
  next = Ready->head;
 
  // If cop has been set (i.e. sys_call has been called before)
  // update its stack top and reinsert it to the ready queue
  if (cop) {
    if (params.op_code == IDLE){
      cop->stack_top = (u32int*)registers;
      insert_pcb(cop);
    }
    else if (params.op_code == EXIT){
      //delete cop
      //free cop
      cop = NULL;
    }
  }
  else {
    //save the original caller's registers 
    //so we can return to it at the end
    old = registers;
  }
  
  // If there is a process ready to run, set it
  // as the currently operating process, else,
  // return to the original caller.
  if (next){
    remove_pcb(next);
    cop = next;
  }
  else {
    return (u32int*)old;
  }
  //End-of-Interrupt
  outb(0x20,0x20);
  return cop->stack_top;
}
