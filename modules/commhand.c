#include <string.h>

#include <core/io.h>
#include <core/serial.h>
#include <mem/heap.h>

#include "mpx_supt.h"
#include "modules.h"
#include "commhand.h"
#include "commands.h"

#define MAX_CMD 64

// Keycodes
#define k_BCK 0x008 //backspace
#define k_DEL 0x07f //delete
#define k_SPC 0x060 //space

// Commonly used character sequences
static char bkspc[2] = {k_BCK,'\0'};
static char blank[2] = {' ','\0'};
//static char delete[2] = {0x7f,'\0'};

// MPX Commands
extern command Commands[MAX_CMD_NUM];
extern alias Aliases[MAX_ALIAS_NUM];

// Globals
char wdir[32] = {'\0'};
char prompt[8] = {'\0'};
char cmd_buf[MAX_CMD] = {'\0'};

/*
  Procedure..: print_welcome
  Description..: Prints the name and version in a welcome message.
*/
void print_welcome(){
  serial_print("\r");
  serial_print("Welcome to ");
  serial_print(SYS_NAME);
  serial_print(" ");
  serial_println(SYS_VERSION);
  serial_println(" ");
  serial_print(prompt);
}  

/*
  Procedure..: commhand
  Description..: MPX command handler. Reads and evaluates user
     input from the default I/O device.
*/
void commhand()
{
  print_welcome();

  int i = 0;
  command *cmd;
  char ch;
  char letter[2] = "\0\0";

  //poll for serial input for now
  for(;;){
    ch = inb(COM1+5)&1; //check for input
    if (ch){
      ch = inb(COM1); //grab byte

      //if backspace character
      if (ch == 0x07f){
	if (i>0){
	  serial_print(bkspc); //backup
	  serial_print(blank); //overwrite
	  serial_print(bkspc); //backup
	  i-=1;
	  cmd_buf[i] = '\0';
	}
      }
      else {
	letter[0] = ch;
	serial_print(letter);
	cmd_buf[i] = ch;
	i+=1;
      }

      //command submitted
      if (ch == '\r' || i >= MAX_CMD){
	
	if (ch == '\r'){
	  cmd_buf[i-1] = '\0';
	}
	else if ( i>=MAX_CMD ){
	  cmd_buf[i] = '\0';
	  serial_println("");
	}
	
	//if more than just a return char
	if ( i>1 ){
	  
	  //check if valid command
	  cmd = valid_command(cmd_buf);	
	  
	  //if it is, evaluate it
	  if (cmd){
	    eval_command(cmd, cmd_buf);
	  }
	  else {
	    serial_print("unknown command '");
	    serial_print(cmd_buf);
	    serial_println("'");
	  }
	}

	//reset command_buffer
	memset(cmd_buf,'\0',MAX_CMD);
	i = 0;
	
	//yield
	sys_req(IDLE);

	serial_print(prompt);
      }
      ch = 0;
    }
  }
}

/* Procedure..: init_commhand */
void init_commhand()
{
  init_commands();
  strcpy(prompt, ">>> ");
  return;
}

/*
  Procedure..: nth_tok
  Description..: Returns the nth token of a character array using
      the given delimeter as the splitting character.
*/
char* nth_tok(const char *s, const int n, const char *delim)
{
  char tmp[MAX_CMD], *tok;
  int i;

  strcpy(tmp,s);

  tok = strtok(tmp,delim);
  for(i=0;i<n;i++)
    tok = strtok(0,delim); //NULL
  
  return tok;
}

/* 
   Procedure..: valid_command
   Description..: Searches through the array of system commands
      and then through the array of user defined aliases. If found,
      returns the command structure, NULL otherwise.
*/
command* valid_command(const char *cmd_buf)
{
  int i=0, j=0;

  char *cmd_name;
  cmd_name = nth_tok(cmd_buf, 0, " ");

  // Check defined commands
  for(i=0; (i < MAX_CMD_NUM); i++)
    if (strcmp(Commands[i].name, cmd_name)==0)
      return &Commands[i];
  
  // Check aliases
  if (i == MAX_CMD_NUM)
    for(j=0; (j < MAX_ALIAS_NUM); j++)
      if (strcmp(Aliases[j].name, cmd_name)==0)
	return Aliases[j].cmd;

  return (void*)NULL;
}

/*
  Procedure..: eval_command
  Description..: Takes a pointer to a command structure and 
     the user input. It then counts the arguments and passes
     the number of arguments (argc) and the user input (argv)
     to the function inside the command structure.
*/
int eval_command(const command *cmd, const char *cmd_buf)
{
  int argc = 0;
  while(nth_tok(cmd_buf,argc," ")) argc++; //count params
  return (*cmd->func)(argc, cmd_buf); //call the function
}
