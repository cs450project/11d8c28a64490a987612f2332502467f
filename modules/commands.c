#include <string.h>
#include <core/serial.h>
#include <mem/heap.h>

#include "mpx_supt.h"
#include "commhand.h"
#include "commands.h"
#include "pcb.h"
#include "queue.h"
#include "com.h"

// Get rid of GCC warnings about not using parameters
#define useargcv if (argc || cmd_buf) while(1) break

#define MAX_ALIAS 16
#define MAX_HIST  16

int NumCommands = 0;
command Commands[256];

int NumAliases = 0;
alias Aliases[MAX_ALIAS];

int NumHist = 0;
char Hist[MAX_HIST];

extern char wdir[32];
extern char prompt[8];

void add_command(const char *name, const char *desc, int (*func)())
{
  NumCommands++;
  strcpy(Commands[NumCommands-1].name, name);
  strcpy(Commands[NumCommands-1].desc, desc);
  Commands[NumCommands-1].func = func;
}

void init_commands()
{
  // Module R1
  add_command("alias","create an alias for a command", cmd_alias);
  add_command("help","display the help file for MPX", cmd_help);
  add_command("prompt","change the prompt symbol", cmd_prompt);
  add_command("version", "display the current version of MPX", cmd_version);
  //date
  //setdate
  add_command("shutdown","shutdown MPX", cmd_shutdown);
  
  // Module R2
  add_command("create","create a new process", cmd_create_pcb);
  add_command("show","show ready queue", cmd_show);
  add_command("delete","delete a process", cmd_delete_pcb);
  add_command("block","block a process", cmd_block_pcb);
  add_command("unblock","unblock a process", cmd_unblock_pcb);
  add_command("resume","resume a process", cmd_resume_pcb);
  add_command("suspend","suspend a process", cmd_suspend_pcb);

  // Module R3
  add_command("load","load a test process", cmd_load_proc);
  add_command("loadr3","load module R3 test processes", cmd_loadr3);
  add_command("yield","call the scheduler", cmd_yield);

  // Module R5
  add_command("loadcomw","load com write test process", cmd_load_comw);
}

/* ---------- R1 Commands ---------- */

int cmd_alias(const int argc, const char *cmd_buf)
{
  useargcv;

  if (argc == 1){ 
    int i;
    for (i=0; i < NumAliases; i++){
      serial_print(Aliases[i].name);
      serial_print("\t");
      serial_println(Aliases[i].cmd->name);
    }
    if (i==0) serial_println("no aliases defined");
  }
  else if (argc == 3){

    char *name = nth_tok(cmd_buf,1," ");
    command *cmd  = valid_command(nth_tok(cmd_buf,2," "));

    if (!cmd){
      serial_println("error: invalid command requested");
    }
    else if (NumAliases >= MAX_ALIAS_NUM){
      serial_println("error: maximum number of aliases defined");
    }
    else {
      strcpy(Aliases[NumAliases].name, name);
      Aliases[NumAliases].cmd = cmd;
      NumAliases += 1;
    }

  }
  else serial_println("usage: alias [new_command] [old_command]");

  return 0;
}

int cmd_help(const int argc, const char *cmd_buf)
{
  useargcv;
  
  int i;
  char *name;

  serial_println("[Command]\t[Description]");
  for(i=0;i<NumCommands;i++){
    name = Commands[i].name;
    serial_print(name);
    
    if (strlen(name) < 12 && strlen(name) >= 8)
      serial_print("\t");
    else if (strlen(name) < 8 && strlen(name) >= 4)
      serial_print("\t\t");
    else
      serial_print("\t\t\t");
    
  serial_println(Commands[i].desc);
  }

  return 0;
}

int cmd_prompt(const int argc, const char *cmd_buf)
{
  useargcv;
  if (argc == 2){
    strcpy(prompt,nth_tok(cmd_buf,1," "));
    strcat(prompt," \0");
  }
  else serial_println("usage: prompt [new prompt]");
  return 0;
}

int cmd_version(const int argc, const char *cmd_buf)
{
  useargcv;
  char version[32] = {'\0'};
  strcat(version, SYS_NAME);
  strcat(version, " Version ");
  strcat(version, SYS_VERSION);
  serial_println(version);
  return 0;
}

/* ---------- R2 Commands ---------- */

int cmd_block_pcb(const int argc, const char *cmd_buf)
{
  useargcv;
  if (argc < 2) serial_println("usage: block [process name]");
  else {
    char *name;
    pcb *p;
    int i;
    for (i=1; i<argc; i++){
      name = nth_tok(cmd_buf,i," ");
      if( (p=find_pcb(name)) ){
	block_pcb(p);
      }
      else {
	serial_print("error: ");
	serial_print(name);
	serial_println(" not found");
      }
    }
  }
  return 0;
}

int cmd_create_pcb(const int argc, const char *cmd_buf)
{
  if (argc == 5){
    char *name   =        nth_tok(cmd_buf,1," ");
    int class    = (int) *nth_tok(cmd_buf,2," ") - 48;
    int status   = (int) *nth_tok(cmd_buf,3," ") - 48;
    int priority = (int) *nth_tok(cmd_buf,4," ") - 48;

    int noerror = 1;

    if ( (class < 1) || (class > 2) ){
      serial_println("class out of range. 1-app, 2-sys.");
      noerror = 0;
    }
    if ( (status < 1) || (status > 4) ){
      serial_println("status out of range. 1r, 2sr, 3b, 4sb.");
      noerror = 0;
    }
    if ( (priority < 0) || (priority > 9) ){
      serial_println("priority out of range. valid: 0-9");
      noerror = 0;
    }
    if ( find_pcb(name) ){
      serial_println("pcb already exists");
      noerror = 0;
    }
    if (noerror) insert_pcb(create_pcb(name, class, status, priority, 1024));
  }
  else serial_println("usage: create [name] [class] [status] [priority]");
  return 0;
}

int cmd_delete_pcb(const int argc, const char *cmd_buf)
{
  if (argc == 2){
    char *name = nth_tok(cmd_buf,1," ");
    pcb *p = find_pcb(name);
    if (p){
      remove_pcb(p);
      //free(p);
    }
    else { serial_println("no pcb with that name found"); }
  }
  return 0;
}

char* qid_to_string(int qid)
{
  switch(qid){
  case READY: return "READY";
  case SREADY: return "SREADY";
  case BLOCKED: return "BLOCKED";
  case SBLOCKED: return "SBLOCKED";
  }
  return "";
}

void print_pcb(pcb *p)
{
  serial_print(p->name);
  serial_print("\t\t");
  serial_print(qid_to_string(p->state));
  serial_print("\t\t");
  serial_println("DO ASCII 2 INT!");
}


int cmd_resume_pcb(const int argc, const char *cmd_buf)
{
  useargcv;
  if (argc < 2) serial_println("usage: resume [process name]");
  else {
    char *name;
    pcb *p;
    int i;
    for (i=1; i<argc; i++){
      name = nth_tok(cmd_buf,i," ");
      if( (p=find_pcb(name)) ){
	resume_pcb(p);
      }
      else {
	serial_print("error: ");
	serial_print(name);
	serial_println(" not found");
      }
    }
  }
  return 0;
}


int cmd_show(const int argc, const char *cmd_buf)
{
  process_queue *queue;
  pcb *p;
  int i;
  int qid = 0;

  serial_println("[Name]\t\t[State]\t\t[Priority]");

  for(i=1;i<argc;i++){
    if      (strcmp("-r", nth_tok(cmd_buf,i," "))==0) qid=READY;
    else if (strcmp("-b", nth_tok(cmd_buf,i," "))==0) qid=BLOCKED;
    else if (strcmp("-s",nth_tok(cmd_buf,i," "))==0) qid=SREADY;
    else if (strcmp("-sb",nth_tok(cmd_buf,i," "))==0) qid=SBLOCKED;
    else if (strcmp("-a", nth_tok(cmd_buf,i," "))==0) qid = ALL;
    else if ( (p = find_pcb(nth_tok(cmd_buf,i," "))) ) print_pcb(p);
  }

  // Show all queues
  if (qid == ALL){
    for(qid=1; qid<5; qid++){
      queue = get_queue(qid);
      p = queue->head;
      if (p){
	print_pcb(p);
	while(p->next){
	  p = p->next;
	  print_pcb(p);
	  }
      }
    }
  }
  
  // Show single queue
  else if (qid){
    queue = get_queue(qid);
    p = queue->head;
    if (p){
      print_pcb(p);
      while(p->next){
	p = p->next;
	print_pcb(p);
      }
    }     
  }

  return 0;
}

int cmd_show_pcb(const int argc, const char *cmd_buf)
{
  pcb *p;
  p = find_pcb(nth_tok(cmd_buf,(argc-1)," ")); 
  if (p) serial_println(p->name);
  else serial_println("not found");
  return 0;
}

int cmd_suspend_pcb(const int argc, const char *cmd_buf)
{
  useargcv;
  if (argc < 2) serial_println("usage: unblock [process name]");
  else {
    char *name;
    pcb *p;
    int i;
    for (i=1; i<argc; i++){
      name = nth_tok(cmd_buf,i," ");
      if( (p=find_pcb(name)) ){
	suspend_pcb(p);
      }
      else {
	serial_print("error: ");
	serial_print(name);
	serial_println(" not found");
      }
    }
  }
  return 0;
}

int cmd_unblock_pcb(const int argc, const char *cmd_buf)
{
  useargcv;
  if (argc < 2) serial_println("usage: unblock [process name]");
  else {
    char *name;
    pcb *p;
    int i;
    for (i=1; i<argc; i++){
      name = nth_tok(cmd_buf,i," ");
      if( (p=find_pcb(name)) ){
	unblock_pcb(p);
      }
      else {
	serial_print("error: ");
	serial_print(name);
	serial_println(" not found");
      }
    }
  }
  return 0;
}

/* ---------- R3 Commands ---------- */

extern pcb* cop;

void test()
{
  while(1){
    serial_print("dispatching ");
    serial_println(cop->name);
    asm volatile ("int $60");
  }
}

int cmd_load_proc(const int argc, const char *cmd_buf)
{
  useargcv;
  char *name = nth_tok(cmd_buf,argc-1," ");
  
  pcb *new_pcb = create_pcb(name, 1, 1, 1, 256);
  context *cp = (context*)(new_pcb->stack_top);
  memset(cp, 0, sizeof(context));
  cp->fs = 0x10;
  cp->gs = 0x10;
  cp->ds = 0x10;
  cp->es = 0x10;
  cp->cs = 0x8;
  cp->ebp = (u32int)(new_pcb->stack);
  cp->esp = (u32int)(new_pcb->stack_top);
  cp->eip = (u32int)test;
  cp->eflags = 0x202;
  insert_pcb(new_pcb);

  return 0;
}

pcb* create_proc(const char *name, void (*func)(), int stack_size)
{
  pcb *new_pcb = create_pcb(name, 1, 1, 1, stack_size);
  context *cp = (context*)(new_pcb->stack_top);
  memset(cp, 0, sizeof(context));
  cp->fs = 0x10;
  cp->gs = 0x10;
  cp->ds = 0x10;
  cp->es = 0x10;
  cp->cs = 0x8;
  cp->ebp = (u32int)(new_pcb->stack);
  cp->esp = (u32int)(new_pcb->stack_top);
  cp->eip = (u32int)func;
  cp->eflags = 0x202;
  return new_pcb;
}

void proc1();
void proc2();
void proc3();
void proc4();
void proc5();

int cmd_loadr3(const int argc, const char *cmd_buf)
{  
  useargcv;
  int i;
 
  char names[5][8] =  { "proc1\0","proc2\0","proc3\0","proc4\0","proc5\0" };
  void (*funcs[5])() = { proc1,  proc2,  proc3,  proc4,  proc5  };
 
  for (i=0; i<5; i++){
    insert_pcb(create_proc(names[i], funcs[i], 256));
  }
  return 0;
}

int cmd_shutdown(const int argc, const char *cmd_buf)
{
  useargcv;
  clear_queues();
  asm volatile ("int $60");
  return 0;
}

int cmd_yield(const int argc, const char *cmd_buf)
{
  useargcv;
  asm volatile ("int $60");
  return 0;
}

//R5

void test_comw()
{
  int eflag;
  open_com(&eflag, 1200);

  int count = 12;
  com_write("Hello, COM2!", &count);

  close_com(COM2);

  serial_println("test_comw completed");
  sys_req(EXIT);
}

int cmd_load_comw(const int argc, const char *cmd_buf)
{
  useargcv;
  insert_pcb(create_proc("comw",test_comw,256));
  return 0;
}
